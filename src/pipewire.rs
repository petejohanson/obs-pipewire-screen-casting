// extern crate glib;
extern crate gstreamer as gst;
extern crate gstreamer_app as gst_app;
extern crate gstreamer_video as gst_video;

use gstreamer::prelude::*;
use std::sync::mpsc::{ Receiver, TryRecvError };
use std::error::Error;

extern crate byte_slice_cast;
use byte_slice_cast::*;

#[derive(Debug, Default)]
pub struct PipewireError {
    description: String,
    cause: Option<gst::Error>,
}

impl std::fmt::Display for PipewireError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(self.description.as_str())
    }
}

impl std::error::Error for PipewireError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self.cause {
            Some(ref c) => Some(c),
            _ => None
        }
    }
}

impl From<gst::StateChangeError> for PipewireError {
    fn from(_: gst::StateChangeError) -> PipewireError {
        PipewireError { description: String::from("State-change error"), ..Default::default() }
    }
}

impl From<gst::Error> for PipewireError {
    fn from(e: gst::Error) -> PipewireError {
        PipewireError { description: e.description().into(), cause: Some(e) }
    }
}

#[derive(Debug)]
pub enum PipeStreamMessage {
    Pause,
    Play,
    Stop
}

pub struct ScreenCastContext {
    pub size: Option<crate::screen_cast::ScreenSize>,
    pub source: *mut crate::libobs::obs_source_t
}

unsafe impl Send for ScreenCastContext {}
unsafe impl Sync for ScreenCastContext {}

fn gst_to_obs_video_format(gst_format: &gst_video::VideoFormat) -> crate::libobs::video_format {
    match gst_format {
        gst_video::VideoFormat::I420 => crate::libobs::video_format_VIDEO_FORMAT_I420,
        gst_video::VideoFormat::Nv12 => crate::libobs::video_format_VIDEO_FORMAT_I420,
        gst_video::VideoFormat::Bgra => crate::libobs::video_format_VIDEO_FORMAT_BGRA,
        gst_video::VideoFormat::Bgrx => crate::libobs::video_format_VIDEO_FORMAT_BGRX,
        gst_video::VideoFormat::Rgba | gst_video::VideoFormat::Rgbx => crate::libobs::video_format_VIDEO_FORMAT_RGBA,
        gst_video::VideoFormat::Uyvy => crate::libobs::video_format_VIDEO_FORMAT_UYVY,
        gst_video::VideoFormat::Yuy2 => crate::libobs::video_format_VIDEO_FORMAT_YUY2,
        gst_video::VideoFormat::Yvyu => crate::libobs::video_format_VIDEO_FORMAT_YVYU,
        _ => crate::libobs::video_format_VIDEO_FORMAT_NONE
    }
}

fn gst_color_matrix_to_obs_colorspace(matrix: &gst_video::VideoColorMatrix) -> crate::libobs::video_colorspace {
    match matrix {
        gst_video::VideoColorMatrix::Bt709 => crate::libobs::video_colorspace_VIDEO_CS_709,
        gst_video::VideoColorMatrix::Bt601 => crate::libobs::video_colorspace_VIDEO_CS_601,
        _ => crate::libobs::video_colorspace_VIDEO_CS_DEFAULT
    }
}

fn gst_color_range_to_obs_range(range: &gst_video::VideoColorRange) -> (crate::libobs::video_range_type, bool) {
    match range {
        gst_video::VideoColorRange::Range0255 => 
            (crate::libobs::video_range_type_VIDEO_RANGE_FULL, true),
        gst_video::VideoColorRange::Range16235 =>
            (crate::libobs::video_range_type_VIDEO_RANGE_PARTIAL, false),
        _ => (crate::libobs::video_range_type_VIDEO_RANGE_DEFAULT, false)
    }
}

fn basic_error(description: &str) -> PipewireError {
    PipewireError { description: String::from(description), ..Default::default() }
}

fn get_source_frame(appsink: &gst_app::AppSink, screen_size: Option<crate::screen_cast::ScreenSize>) -> Result<crate::libobs::obs_source_frame, PipewireError> {
    gst_app::AppSink::pull_sample(appsink)
        .ok_or(basic_error("Failed to get current sample from screen casting stream"))
        .and_then(|s| {
            let buff = s.get_buffer().ok_or(basic_error("Failed to get the screen casting buffer"))?;
            let map = buff.map_readable().ok_or(basic_error("Failed to map data from the screen casting buffer"))?;

            let samples = map.as_slice_of::<u8>().map_err(|_e| basic_error("Failed to get bytes from screen casting buffer "))?;

            let vi = s.get_caps().and_then(|caps| gst_video::VideoInfo::from_caps(caps.as_ref())).ok_or(basic_error("Failed to get video info from the sample frame"))?;

            let vi_stride = vi.stride();
            let linesize: [u32; 8] = [
                vi_stride.get(0).map(|i| *i as u32).unwrap_or(0u32),
                vi_stride.get(1).map(|i| *i as u32).unwrap_or(0u32),
                vi_stride.get(2).map(|i| *i as u32).unwrap_or(0u32),
                0u32, 0u32, 0u32, 0u32, 0u32];

            let offsets = vi.offset();

            let data: [*mut u8; 8] = [
                offsets.get(0).map(|&o| unsafe { samples.as_ptr().offset(o as isize) }).unwrap_or(std::ptr::null_mut()) as *mut u8,
                offsets.get(1).map(|&o| unsafe { samples.as_ptr().offset(o as isize) }).unwrap_or(std::ptr::null_mut()) as *mut u8,
                offsets.get(2).map(|&o| unsafe { samples.as_ptr().offset(o as isize) }).unwrap_or(std::ptr::null_mut()) as *mut u8,
                std::ptr::null_mut(),
                std::ptr::null_mut(),
                std::ptr::null_mut(),
                std::ptr::null_mut(),
                std::ptr::null_mut(),
            ];

            let (range, full_range) = gst_color_range_to_obs_range(&vi.colorimetry().range());
            let cs = gst_color_matrix_to_obs_colorspace(&vi.colorimetry().matrix());
            let format = gst_to_obs_video_format(&vi.format());

            let mut color_matrix: [f32; 16] = Default::default();
            let mut color_range_min: [f32; 3] = Default::default();
            let mut color_range_max: [f32; 3] = Default::default();
            unsafe {
                crate::libobs::video_format_get_parameters(cs, range, color_matrix.as_mut_ptr(), color_range_min.as_mut_ptr(), color_range_max.as_mut_ptr());
            }

            let (width, height) = screen_size.unwrap_or((vi.width(), vi.height()));

            let frame = crate::libobs::obs_source_frame {
                timestamp: buff.get_pts().nanoseconds().unwrap_or(0),
                format,
                width,
                height,
                linesize,
                data,
                color_matrix,
                color_range_min,
                color_range_max,
                full_range,
                ..Default::default()
            };

            Ok(frame)
        })
}

pub fn pipe_to_source(node_id: u32, fd: String, ctx: ScreenCastContext, rx: Receiver<PipeStreamMessage>) -> Result<(), PipewireError> {
    let pipeline = gst::parse_launch(&format!("pipewiresrc fd={} path={} ! videoconvert ! video/x-raw, format={{I420,NV12,BGRA,RGBA,YUY2,YVYU,UYVY}} ! appsink name=video_appsink", fd, node_id))?;

    let pipeline = pipeline.dynamic_cast::<gst::Pipeline>().unwrap();
    
    let sink = pipeline.get_by_name("video_appsink").unwrap();
    let sink = sink.dynamic_cast::<gst_app::AppSink>().unwrap();

    sink.set_callbacks(gst_app::AppSinkCallbacks::new().new_sample(move |arg| {
        get_source_frame(&arg, ctx.size)
            .map(|f| unsafe { crate::libobs::obs_source_output_video(ctx.source, &f) })
            .map(|_| gst::FlowSuccess::Ok)
            .map_err(|_| gst::FlowError::Error)
    }).build());

    let bus = pipeline.get_bus().unwrap();

    'main: loop {
        match rx.try_recv() {
            Err(TryRecvError::Disconnected) | Ok(PipeStreamMessage::Stop) => {
                break 'main;
            },
            Ok(PipeStreamMessage::Pause) => {
                pipeline.set_state(gst::State::Paused)?;
            },
            Ok(PipeStreamMessage::Play) => {
                pipeline.set_state(gst::State::Playing)?;
            },
            _ => {
                if let Some(msg) = bus.timed_pop(100 * gst::MSECOND) {
                    use gstreamer::MessageView;

                    match msg.view() {
                        MessageView::Eos(_) => break 'main,
                        MessageView::Error(err) => {
                            Err(PipewireError{
                                description: format!(
                                    "Error from {:?}: {} ({:?})",
                                    err.get_src().map(|s| s.get_path_string()),
                                    err.get_error(),
                                    err.get_debug()
                                ),
                                cause: Some(err.get_error())
                            })?;
                        }
                        _ => (),
                    }
                }
            }
        }
    }

    pipeline.set_state(gst::State::Null)?;

    Ok(())
}