use dbus::{BusType, Connection, ConnectionItem, ConnPath};
use std::collections::HashMap;
use std::error::Error;

pub type ScreenSize = (u32, u32);

#[derive(Debug)]
pub struct ScreenCast {
    pub conn: Connection,
    pub node_id: u32,
    pub size: Option<ScreenSize>,
    pub fd: dbus::OwnedFd
}

#[derive(Debug, Default)]
pub struct ScreenCastError {
    description: String,
    cause: Option<dbus::Error>,
}

impl std::fmt::Display for ScreenCastError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(self.description.as_str())
    }
}

impl std::error::Error for ScreenCastError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self.cause {
            Some(ref c) => Some(c),
            _ => None
        }
    }
}

impl From<dbus::Error> for ScreenCastError {
    fn from(err: dbus::Error) -> ScreenCastError {
        ScreenCastError { description: String::from(err.description()), cause: Some(err) }
    }
}

impl From<String> for ScreenCastError {
    fn from(description: String) -> ScreenCastError {
        ScreenCastError { description, ..Default::default() }
    }
}

fn simple_error(s: &str) -> ScreenCastError {
    ScreenCastError { description: String::from(s), ..Default::default() }
}

struct TokenState {
    name: String,
    count: i32
}

fn next_request(TokenState { name, count }: TokenState) -> TokenState {
    TokenState { name, count: count + 1 }
}

fn new_path(request_type: String, s: TokenState) -> ((String, String, TokenState)) {
    let new_state = next_request(s);

    let token = format!("u{}", new_state.count);
    let path = format!("/org/freedesktop/portal/desktop/{type}/{name}/{token}", type = request_type, name = new_state.name, token = token);
    
    (path, token, new_state)
}

fn is_portal_response(m: &dbus::Message) -> bool {
    &*m.interface().unwrap() == "org.freedesktop.portal.Request" &&
        &*m.member().unwrap() == "Response"
}

type SimpleHashResponse<'a> = HashMap<&'a str, dbus::arg::Variant<&'a str>>;

enum PortalResponseError {
    Cancelled,
    OtherEnded,
    DataError,
}

fn parse_portal_response<'a, T>(m: &'a dbus::Message) -> Result<T, PortalResponseError> where T: dbus::arg::Get<'a> {
    let (status, data) = m.get2::<u32, T>();

    match status.unwrap_or(2) {
        0 => data.ok_or(PortalResponseError::DataError),
        1 => Err(PortalResponseError::Cancelled),
        _ => Err(PortalResponseError::OtherEnded)
    }
}

fn get_portal_response(c: &Connection) -> Option<dbus::Message> {
    c
        .iter(100)
        .filter_map(|ci| if let ConnectionItem::Signal(s) = ci { Some(s) } else { None })
        .filter(|m| is_portal_response(&m))
        .next()
}

fn start_session(p: &ConnPath<&Connection>, session_state: TokenState) -> Result<(String, TokenState), ScreenCastError> {
    let (path, token, new_state) = new_path(String::from("session"), session_state);

    use crate::portal::screen_cast::ScreenCast;

    let mut map = HashMap::new();
    let token_arg = Box::new(token) as Box<dbus::arg::RefArg>;
    map.insert("session_handle_token", dbus::arg::Variant(token_arg));
    p.conn.add_match(&format!("interface='org.freedesktop.portal.Request',member='Response',sender='org.freedesktop.portal.Desktop',path='{}'", path))?;
    let _resp_path = p.create_session(map)?;

    get_portal_response(&p.conn)
        .ok_or(simple_error("No response to request to start a screen cast session"))
        .and_then(|m| {
            parse_portal_response::<SimpleHashResponse>(&m)
                .map_err(|_e| simple_error("No response to request to start a screen cast session")) // TODO: Actually map the response type!
                .and_then(|d| d.get("session_handle").map(|h| (String::from(h.0), new_state)).ok_or(simple_error("Unable to retrieve screen cast session details")))
        })
}

fn select_sources(desktop_path: &ConnPath<&Connection>, session_path: dbus::Path, request_state: TokenState) -> Result<TokenState, ScreenCastError> {
    let (path, token, new_state) = new_path(String::from("request"), request_state);

    use crate::portal::screen_cast::ScreenCast;

    let mut map = std::collections::HashMap::new();
    let token_arg = Box::new(token) as Box<dbus::arg::RefArg>;
    let type_arg = Box::new(1) as Box<dbus::arg::RefArg>;
    map.insert("handle_token", dbus::arg::Variant(token_arg));
    map.insert("type", dbus::arg::Variant(type_arg));

    desktop_path.conn.add_match(&format!("interface='org.freedesktop.portal.Request',member='Response',sender='org.freedesktop.portal.Desktop',path='{}'", path))?;
    let _resp = desktop_path.select_sources(session_path, map)?;

    get_portal_response(&desktop_path.conn)
        .ok_or(simple_error("No response to request to select screen cast sources"))
        .and_then(|m| {
            parse_portal_response::<SimpleHashResponse>(&m)
                .map_err(|_e| simple_error("No response to request to select screen cast sources")) // TODO: Actually map the response type!
                .map(|_| new_state)
        })
}

type StartStreamResponse<'a> = HashMap<&'a str, dbus::arg::Variant<Vec<(u32, HashMap<&'a str, dbus::arg::Variant<(i32, i32)>>)>>>;
fn start_recording(desktop_path: &ConnPath<&Connection>, sp: dbus::Path, request_state: TokenState) -> Result<(u32, Option<ScreenSize>, TokenState), ScreenCastError> {
    let (path, token, new_state) = new_path(String::from("request"), request_state);

    use crate::portal::screen_cast::ScreenCast;

    let mut map = std::collections::HashMap::new();
    let token_arg = Box::new(token) as Box<dbus::arg::RefArg>;
    map.insert("handle_token", dbus::arg::Variant(token_arg));

    desktop_path.conn.add_match(&format!("interface='org.freedesktop.portal.Request',member='Response',sender='org.freedesktop.portal.Desktop',path='{}'", path))?;
    desktop_path.start(sp, "", map)?;

    get_portal_response(&desktop_path.conn)
        .ok_or(simple_error("No response to request to start screen casting"))
        .and_then(|m| {
            parse_portal_response::<StartStreamResponse>(&m)
                .map_err(|_e| simple_error("No response to request to start screen casting")) // TODO: Actually map the response type!
                .and_then(|d| {
                    d.get("streams").map(|h| h.0.clone()).and_then(|s| s.get(0).map(|(node_id, opts)| {
                        (node_id.clone(), opts.get("size").map(|v| v.0).map(|(w,h)| (w as u32, h as u32)), new_state)
                    })).ok_or(simple_error("Unable to retrieve screen cast stream details"))
                })
        })
}

fn open_pipewire_remote(portal: &ConnPath<&Connection>, sp: dbus::Path) -> Result<dbus::OwnedFd, ScreenCastError> {
    use crate::portal::screen_cast::ScreenCast;

    portal.open_pipe_wire_remote(sp, HashMap::default()).map_err(|e| e.into())
}

pub fn get_screencast() -> Result<ScreenCast, ScreenCastError> {
    let bus = Connection::get_private(BusType::Session)?;

    let name = bus.unique_name().chars().skip(1).collect::<String>().replace(".", "_");

    let portal = bus.with_path("org.freedesktop.portal.Desktop", "/org/freedesktop/portal/desktop", 3200);

    let (session_path_str, _session_token) = start_session(&portal, TokenState { name: name.clone(), count: 0 })?;

    let session_path = dbus::Path::new(session_path_str)?;

    let request_token = select_sources(&portal, session_path.clone(), TokenState { name: name.clone(), count: 0 })?;

    let (node_id, size, _request_token2) = start_recording(&portal, session_path.clone(), request_token)?;

    let fd = open_pipewire_remote(&portal, session_path.clone())?;

    Ok(ScreenCast { node_id, fd, size, conn: bus })
}

