use std::ffi::{ CString, c_void };
use std::os::raw::c_char;
use std::sync::mpsc::{ Sender };

use crate::libobs::{ obs_data, obs_data_t, obs_properties, obs_source_t, obs_source_info };

#[repr(C)]
struct UserData {
    tx: Sender<crate::pipewire::PipeStreamMessage>,
    thread: std::thread::JoinHandle<()>,
}

extern fn get_name(_type_info: *mut c_void) -> *const c_char {
    CString::new("Desktop Sharing (Portal Desktop/PipeWire)").expect("CString::new failed").into_raw()
}

fn log(level: u32, s: &std::fmt::Display) -> () {
    unsafe {
        crate::libobs::blog(level as i32, CString::new(format!("{}", s).as_str()).unwrap().into_raw());
    }
}

fn start_screencast(context: crate::pipewire::ScreenCastContext) -> (std::thread::JoinHandle<()>, std::sync::mpsc::Sender<crate::pipewire::PipeStreamMessage>) {
    let (tx, rx) =std::sync::mpsc::channel();

    (std::thread::spawn(move || {
        crate::screen_cast::get_screencast().map(|sc| {
            crate::pipewire::pipe_to_source(sc.node_id, format!("{}", sc.fd.into_fd()), crate::pipewire::ScreenCastContext { size: sc.size, ..context }, rx).unwrap()
        }).unwrap_or_else(|e| log(crate::libobs::LOG_ERROR, &e))
    }), tx)
}

extern fn create(_settings: *mut obs_data_t, source: *mut obs_source_t) -> *mut c_void {
    let ctx = crate::pipewire::ScreenCastContext { source, size: None };
    let (thread, tx) = start_screencast(ctx);

    let data = UserData { tx, thread };

    let boxed = Box::new(data);

    Box::into_raw(boxed) as *mut c_void
}

unsafe fn get_user_data(user_data_ptr: *mut c_void) -> Box<UserData> {
    Box::from_raw(user_data_ptr as *mut UserData)
}

unsafe extern fn destroy(user_data_ptr: *mut c_void) -> () {
    let user_data = get_user_data(user_data_ptr);

    let thread = user_data.thread;
    user_data.tx.send(crate::pipewire::PipeStreamMessage::Stop).map(|_| {
        thread.join().unwrap();
    }).unwrap_or(())
}

unsafe fn with_user_data<F>(user_data_ptr: *mut c_void, cb: F) -> *mut c_void  where F: FnOnce(&UserData) -> () {
    let user_data = get_user_data(user_data_ptr);

    cb(&user_data);

    // We re-release with `into_raw` since this still is held by OBS for now
    Box::into_raw(user_data) as *mut c_void
}

unsafe extern fn show(user_data_ptr: *mut c_void) -> () {
    with_user_data(user_data_ptr, |d| d.tx.send(crate::pipewire::PipeStreamMessage::Play).unwrap());
}

unsafe extern fn hide(user_data_ptr: *mut c_void) -> () {
    with_user_data(user_data_ptr, |d| d.tx.send(crate::pipewire::PipeStreamMessage::Pause).unwrap());
}

extern fn activate(_user_data_ptr: *mut c_void) -> () {
}

extern fn deactivate(_user_data_ptr: *mut c_void) -> () {
}

extern fn get_defaults(_data: *mut obs_data) -> () {
}

extern fn get_properties(_user_data: *mut c_void) -> *mut obs_properties {
    unsafe {
        let props = crate::libobs::obs_properties_create();

        props
    }
}

pub fn get_source_info() -> obs_source_info {
    obs_source_info {
        id: CString::new("pipewire-screen-casting-source").expect("CString::new failed").into_raw(),
        type_: crate::libobs::obs_source_type_OBS_SOURCE_TYPE_INPUT,
        output_flags: crate::libobs::OBS_SOURCE_ASYNC_VIDEO | crate::libobs::OBS_SOURCE_DO_NOT_DUPLICATE,
        get_name: Some(get_name),
        create: Some(create),
        destroy: Some(destroy),
        show: Some(show),
        hide: Some(hide),
        activate: Some(activate),
        deactivate: Some(deactivate),
        get_defaults: Some(get_defaults),
        get_properties: Some(get_properties),
        ..Default::default()
    }
}